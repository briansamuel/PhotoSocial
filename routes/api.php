<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
    Route::prefix('v1')->group(function () {
        Route::post('auth/login', 'AuthController@login');
        Route::post('auth/register', 'AuthController@register');

        Route::group(['middleware' => 'auth.jwt'], function () {

            Route::get('auth/user', 'AuthController@user');


             // Feeds Api
            Route::prefix('feeds')->group(function () {
                Route::get('me', 'FeedController@feedsByUser');
                Route::get('friends', 'FeedController@feedsByFriends');
            });
           
           
        });
    });
});
