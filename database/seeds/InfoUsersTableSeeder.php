<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InfoUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker\Factory::create();

        $limit = 60;
      
        
        for ($i = 1; $i <= $limit; $i++) {
            $min = rand(1, 200)*100000;
            $max = $min + rand(1, 200)*100000;
            DB::table('info_users')->insert([
                'user_id' => $i,
                'professions' => $faker->randomElement(['photographer', 'photo_editor', 'video_editor', 'model', 'guest', 'designer', 'mua', 'own_studio']),
                'achievements' => json_encode(['']),
                'phone' => $faker->randomElement(['091', '092', '093', '033', '035', '097', '038']).$faker->randomNumber(7, false),
                'website' => $faker->domainName(),
                'current_location' => json_encode(['address' => $faker->city(), 'latitude' => $faker->latitude(), 'longitude' => $faker->longitude()]),
                'price_range' => json_encode(['min' => $min, 'max' => $max]),
                'created_at' => $faker->dateTime(now(), 'UTC'),
                'updated_at' => now(),

            ]);
        }
    }
}
