<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        $limit = 60;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([
                'full_name' => $faker->name,
                'email' => $faker->unique()->email,
                'password' => bcrypt('admin@123'),
                'birth_date' => $faker->dateTime(now(), 'UTC'),
                'avatar' => $faker->imageUrl(200, 200, null),
                'cover' => $faker->imageUrl(800, 600, null),
                'last_login' => $faker->dateTime(now(), 'UTC'),
                'description' => $faker->sentence(10, true),
                'created_at' => $faker->dateTimeBetween('-2 years', now(), 'UTC'),
                'updated_at' => now(),
            ]);
        }
    }
}
