<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class FeedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        $limit = 5000;
        
        $date = strtotime('2016-01-01');
        $time = date('Y-m-d H:m:s', $date);

        for ($i = 0; $i < $limit; $i++) {
            $user_id = rand(1, 60);
            $photo_ids =  DB::table('photos')->select(['id', 'photo_title', 'photo_url', 'photo_location'])->where('user_id', $user_id)->get()->toArray();
            if(!empty($photo_ids)) {
                $feed_photo = $faker->randomElements($photo_ids, rand(1, count($photo_ids)));
            } else {
                $feed_photo = [];
            }
            $hours_rand = rand(1, 4);
            $minutes_rand = rand(1, 60);
            $second_rand = rand(1, 60);
            $created_at = strtotime("+{$hours_rand} hour +{$minutes_rand} minute +{$second_rand} second", $date);
            $date = $created_at;    
            $time = date('Y-m-d H:m:s', $date);
            DB::table('feeds')->insert([
               'user_id' => $user_id,
               'feed_content' => $faker->sentence(20, true),
               'feed_photos' => json_encode($feed_photo),
               'status' => $faker->randomElement(['publish','private', 'block', 'delete']),
               'created_at' => $time,
               'updated_at' => now(),
            ]);
        }
    }
}
