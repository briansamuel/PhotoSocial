<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FriendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker\Factory::create();

        $limit = 60;

        for ($i = 1; $i <= $limit; $i++) {
            for ($j = 1; $j <= $limit; $j++) {
                DB::table('friends')->insert([
                    'user_id' => $i,
                    'friend_id' => $j,
                    'created_at' => $faker->dateTimeBetween('-2 years', now(), 'UTC'),
                    'updated_at' => now(),
                ]);
            }
        }
    }
}
