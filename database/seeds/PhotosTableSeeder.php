<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        $limit = 1000;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('photos')->insert([
               'user_id' => rand(1, 60),
               'photo_title' => $faker->sentence(10, true),
               'photo_description' => $faker->sentence(20, true),
               'photo_url' => $faker->imageUrl(),
               'photo_location' => json_encode(['address' => $faker->city(), 'latitude' => $faker->latitude(), 'longitude' => $faker->longitude()]),
               'status' => $faker->randomElement(['publish','private', 'block', 'delete']),
               'created_at' => $faker->dateTimeBetween('-2 years', now(), 'UTC'),
               'updated_at' => now(),
            ]);
        }
    }
}
