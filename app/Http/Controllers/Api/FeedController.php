<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Helpers\UploadImage;
use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\FeedService;
use App\Services\FriendService;
use Exception;
use Illuminate\Http\Request;
use Session;

class FeedController extends Controller
{
    protected $request;
    protected $agentService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator, FeedService $feedService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->feedService = $feedService;
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
        return view('admin.agents.index');
    }


    /**
     * METHOD feedsByUser - Feeds By Users
     *
     * @return void
     */

    public function feedsByUser()
    {   
       
        try {
            $current_user = auth()->user();
            $params = $this->request->all();
            $json = [];

            $result = FeedService::getMany(['*'], ['page' => 0, 'perpage' => 20], [], ['user_id' => $current_user->id]);
            if(!empty($result)) {
                foreach($result as $key => $feed) {
                    $photos = $feed->feed_photos ? json_decode($feed->feed_photos) : null;
                    if(!empty($photos)) {
                        foreach($photos as $key_p => $photo) {
                            $photo->photo_location = json_decode($photo->photo_location);     
                        }
                       
                    }
                    $feed->feed_photos = $photos;
                }
            }
            $json['status'] = 'success';
            $json['data'] = $result;
        } catch(Exception $e) {
            $json['status'] = 'fail';
            $json['message'] = $e->getMessage();
        }
       

        return response()->json($json);
    }

    /**
     * METHOD feedsByUser - Feeds From user's friends
     *
     * @return void
     */

    public function feedsByFriends()
    {
        try {
            $current_user = auth()->user();
            $params = $this->request->only('page', 'perpage');
            $page = $params['page'] ?? 0;
            $perpage = $params['perpage'] ?? 20;
            $json = [];
            $friends = FriendService::getAll(['friend_id'], [], ['user_id' => $current_user->id]);

            $result = FeedService::getMany(['*'], ['page' => $page, 'perpage' => $perpage], [], ['user_ids' => $friends->pluck('friend_id')->toArray()]);
            if(!empty($result)) {
                foreach($result as $key => $feed) {
                    $photos = $feed->feed_photos ? json_decode($feed->feed_photos) : null;
                    if(!empty($photos)) {
                        foreach($photos as $key_p => $photo) {
                            $photo->photo_location = json_decode($photo->photo_location);     
                        }
                       
                    }
                    $feed->feed_photos = $photos;
                }
            }
            $json['status'] = 'success';
            $json['data'] = $result;
        } catch(Exception $e) {
            $json['status'] = 'fail';
            $json['message'] = $e->getMessage();
        }
       

        return response()->json($json);
    }
}
