<?php

namespace App\Http\Controllers\Api;

use JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    // login request api
    public function login(Request $request)
    {
        $token = null;
        

        if ((!$token = JWTAuth::attempt($request->only('email', 'password')))) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }

       
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth('api')->factory()->getTTL() * 60,
        ]);
    }
    
    // register request api
    public function register(Request $request) {

    }

    public function user(Request $request) {
        $current_user = Auth::user();
        return response()->json($current_user);
    }

    public function logout()
    {
        Auth::logout();

        return response()->json([], 204);
    }
}