<?php

namespace App\Http\Controllers\FrontSite;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ContactService;
use App\Services\ValidationService;

class ContactController extends Controller
{
    //
    //
    protected $request;
    protected $contactService;
    protected $validator;

    function __construct(Request $request, ContactService $contactService, ValidationService $validator)
    {
        $this->request = $request;
        $this->contactService = $contactService;
        $this->validator = $validator;
    }

    public function contact()
    {
        
        return view('frontsite.pages.contact-page');
    }

    public function contactAction()
    {
        $params = $this->request->only('name', 'email', 'phone_number', 'content');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_frontend_agent_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }

        $add = ContactService::addFromFrontEnd($params);
        if ($add) {
            $data['success'] = true;
            $data['message'] = "Phản hồi của bạn đã được gửi đi thành công!!!";
        } else {
            $data['message'] = "Có lỗi xảy ra, vui lòng thử lại sau!";
        }

        return response()->json($data);
    }
}
