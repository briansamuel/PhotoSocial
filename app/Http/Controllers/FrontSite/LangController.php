<?php

namespace App\Http\Controllers\FrontSite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;

class LangController extends Controller
{
   private $langActive = [
       'vi',
       'en',
   ];
   public function changeLang($locale)
   {
       if (in_array($locale, $this->langActive)) {
            
            App::setLocale($locale);
            session()->put('locale', $locale);
            return redirect()->back();
       }
   }
}
