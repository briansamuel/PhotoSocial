<?php

namespace App\Http\Controllers\FrontSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\HostService;
use App\Services\ProvinceService;
use App\Services\DistrictService;
use App\Services\ReviewService;
class BookingController extends Controller
{
    //
    //
    protected $request;
    protected $hostService;
    protected $provinceService;
    protected $reviewService;

    function __construct(Request $request, HostService $hostService, ProvinceService $provinceService, DistrictService $districtService, ReviewService $reviewService)
    {
        $this->request = $request;
        $this->hostService = $hostService;
        $this->provinceService = $provinceService;
        $this->districtService = $districtService;
        $this->reviewService = $reviewService;
    }


    /**
     * ======================
     * Method:: View Booking
     * ======================
     */

    public function index()
    {
    
        $params = array(
            'pagination' => array(
                'perpage' => 8,
                'page' => 0,
                
            ),
            'sort' => array(
                'field' => 'id',
                'sort' => 'DESC'
            )
            
        );
        $hosts = $this->hostService->getList($params);
        foreach($hosts['data'] as $host) {
            $reviews = $this->reviewService->getAll(['rating_review'], array('host_id' => $host->id));
            $host->user_rating = $reviews->count();
        }
        $data['hosts'] = $hosts['data'];
        
        return view('frontsite.booking.index', $data);
    }

    /**
     * ======================
     * Method:: View Search Hotel
     * ======================
     */

    public function search()
    {
        $params = array(
            'pagination' => array(
                'perpage' => 5,
                'page' => 0,
            )
        );
        $hosts = $this->hostService->getList($params);
        $type_count = $this->hostService->countGroupBy('host_type');
        $data['hosts'] = $hosts;
        $data['type_count'] = $type_count;
        dd($data);
        return view('frontsite.booking.list-hotel', $data);
    }

    public function searchAjax() {
        $input = $this->request->all();
        $q = isset($input['q']) ? $input['q'] : null;
        $pagination = array(
            'perpage' => 5,
            'page' => 0,
        );
        $filter = array('host_name' => $q);
        $hosts = $this->hostService->filterHost(['id', 'host_name'], $pagination, null, $filter);
        $data = [];
        foreach($hosts as $host) {
            $data[] = array(
                'id' => $host->id,
                'text' => $host->host_name,
                'type' => 'host_id',
            );
        }

        $filter_p = array(
            '_name' => $q, 
            'perpage' => 5,
            'page' => 0,
        );
        $provinces = $this->provinceService->getMany(['id', '_name'], $filter_p);
        $data_p = [];
        foreach($provinces as $province) {
            $data_p[] = array(
                'id' => $province->id,
                'text' => $province->_name,
                'type' => 'province_id',
            );
        }

        
        $districts = $this->districtService->getMany(['id', '_name', '_prefix', '_province_id'], $filter_p);

        foreach($districts as $district) {
            $province = $this->provinceService->findByKey('id', $district->_province_id);
            $data_p[] = array(
                'id' => $district->id,
                'text' => trim($district->_prefix.' '.$district->_name.', '.$province->_name),
                'type' => 'district_id',
            );
        }
        $result = array(
            array(
                'text' => 'Khách Sạn',
                'children' => $data,
            ),
            array(
                'text' => 'Địa điểm',
                'children' => $data_p,
            )
        );
        return response()->json($result);
    }
    /**
     * ======================
     * Method:: View Search Hotel
     * ======================
     */

    public function infoHost()
    {
    
        return view('frontsite.booking.detail_hotel');
    }
}
