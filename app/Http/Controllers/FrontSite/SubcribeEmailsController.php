<?php

namespace App\Http\Controllers\FrontSite;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Services\SubcribeEmailsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ValidationService;

class SubcribeEmailsController extends Controller
{
    //
    //
    protected $request;
    protected $contactService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;
    }

    public function addAction()
    {
        $params = $this->request->only('email');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_frontend_subcribe_email_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }

        if (SubcribeEmailsService::checkEmailExist($params['email'])) {
            $data['success'] = false;
            $data['message'] = 'Bạn đã đăng ký nhận ưu đãi trước đó rồi';

            return $data;
        }

        $add = SubcribeEmailsService::addFromFrontEnd($params);
        if ($add) {
            $data['success'] = true;
            $data['message'] = "Bạn đã đăng ký nhận ưu đãi thành công!!!";
        } else {
            $data['message'] = "Có lỗi xảy ra, vui lòng thử lại sau!";
        }

        return response()->json($data);
    }
}
