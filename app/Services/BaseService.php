<?php
namespace App\Services;

use App\Models\BaseModel;

class BaseService
{
    protected $model;
    
    // function __construct(BaseModel $baseModel)
    // {
    //     $this->model = $baseModel;

    // }

    public static function totalRows($params) {
        $result = self::$model::totalRows($params);
        return $result;
	}
	
	public static function getMany($columns, $pagination, $sort, $filter)
	{
		$result = self::$model->getMany($columns, $pagination, $sort, $filter);
        return $result ? $result : [];
	}


	public static function findByKey($key, $value)
	{
        $result = self::$model->findByKey($key, $value);
        return $result ? $result : [];
	}


}