<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FeedModel extends BaseModel
{
    //
    protected static $table = 'feeds';

    public static function getMany($columns = ['*'],$pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];

        $query = DB::table(self::$table)->select($columns)->skip($offset)->take($pagination['perpage']);

        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        } else {
            $query->where('status', 'like', 'publish');
        }

        if(isset($filter['user_id']) && $filter['user_id'] != ""){
            $query->where('user_id', '=', $filter['user_id']);
        }

        if(isset($filter['user_ids']) && $filter['user_ids'] != ""){

            $query->whereIn('user_id', $filter['user_ids']);
        }
        
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }

        if(isset($sort['field']) && $sort['field'] != ""){
            $query->orderBy($sort['field'], $sort['sort']);
        } else {
            $query->orderBy('created_at', 'desc');
        }

        return $query->get();
    }

    public static function totalRows($filter) {

        $query = DB::table(self::$table);
        
        if(isset($filter['status']) && $filter['status'] != ""){
            $query->where('status', '=', $filter['status']);
        } else {
            $query->where('status', 'like', 'publish');
        }

        if(isset($filter['user_id']) && $filter['user_id'] != ""){
            $query->where('user_id', '=', $filter['user_id']);
        }
      
        return $query->count();

    }
}
