<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BaseModel 
{
    //
    protected static $table;
    
    public static function findByKey($key, $value, $columns = ['*'])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }

    public static function findByCondition($condition, $columns = ['*'])
    {
        $data = DB::table(self::$table)->select($columns)->where($condition)->first();
        return $data ? $data : [];
    }

    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function updateView($id, $view)
    {
        return DB::table(self::$table)->where('id', $id)->increment('total_view', $view);

    }

    public static function updateMany($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }

    public static function deleteMany($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }
    
}
